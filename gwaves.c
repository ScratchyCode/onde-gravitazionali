// Coded by Pietro Squilla
// Test segnali onde gravitazionali
/*
    tempi consigliati in ms:
    t0 = 0.4
    sigma = 0.03
    T0 = 0.015
    t = 1
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TAU 0.2
#define DT 0.00001
#define GRAFICO // grafico dell'onda fino al t specificato

double input(double min, double max);
double segnale(double t, double t0, double periodo, double sigma);
double T(double t, double T0);
void ctrlPtr(void *ptr);

int main(){
    double t, t0, sigma, T0;
    
    // input costanti
    printf("\nInserisci (in ms) il tempo di massima ampiezza t0: ");
    t0 = input(0,10);
    
    printf("\nInserisci (in ms) la durata dell'impulso sigma: ");
    sigma = input(0,10);
    
    printf("\nInserisci (in ms) il periodo caratteristico delle oscillazioni T0: ");
    T0 = input(0,10);
    
    // tempo generico
    printf("\nInserisci il tempo di acquisizione dati t: ");
    t = input(0,10*t0);
    
    double ampiezza;
    //ampiezza = segnale(t,t0,T,sigma);
    ampiezza = segnale(t,t0,T(t,T0),sigma);
    printf("\nTempo: %.1lf\tAmpiezza: %.3lf\n",t,ampiezza);
    
    // trovo i punti da plottare
    #ifdef GRAFICO
    double tmax = t;
    FILE *output = fopen("segnale.dat","w");
    ctrlPtr(output);
    
    t = 0;
    while(t <= tmax){
        ampiezza = segnale(t,t0,T(t,T0),sigma);
        fprintf(output,"%lf %lf\n",t,ampiezza);
        t = t + DT;
    }
    
    fclose(output);
    #endif
    
    // echo "plot 'segnale.dat' u 1:2 w d" | gnuplot -p
    return 0;
}

double input(double min, double max){
    double x;
    
    do{
        scanf("%lf",&x);
        if(x < min || x > max){
            printf("\nL'input deve essere compreso strettamente tra %lf e %lf.\nRiprova: ",min,max);
        }
    }while(x <= min || x >= max);
    
    return x;
}

double segnale(double t, double t0, double periodo, double sigma){
    double G;
    
    G = exp( (-(1/2))*(pow((t-t0),2.)/pow(sigma,2.)) ) * sin(2*M_PI*(t/periodo));
    
    return G;
}

double T(double t, double T0){
    return (T0*exp(-t/TAU));
}

void ctrlPtr(void *ptr){
    if(!ptr){
        perror("\n\nError");
        fprintf(stderr,"\n");
        exit(0);
    }
}
